
#------------------------------------------------------
#Import Libraries
#------------------------------------------------------

from BA_Graph import *



directory_files=os.listdir('.')

if ('MNIST_TRAIN_IMAGES.npy' in directory_files and 
	'MNIST_TEST_IMAGES.npy' in directory_files and
	'MNIST_TRAIN_LABELS.npy' in directory_files and
	'MNIST_TEST_LABELS.npy' in directory_files):
	download_images_again = raw_input('Do you want to download MNIST data again? (y/n):\n')
	if download_images_again != 'n':
		from download_format_images import * #we need to download the data and convert to .npy format...
else:
	from download_format_images import * #we need to download the data and convert to .npy format...	


#------------------------------------------------------
#Main class declaration
#------------------------------------------------------
class BA_NN(BA_Graph):

	#------------------------------------------------------
	#Initialisation functions, including loading and saving
	#------------------------------------------------------

	def __init__(self):
		self.load_graph()
		self.import_training_data()
		self.neurons = self.vertices
		self.NUM_OF_NEURONS = len(self.neurons)

		'Load prev weights if applicable!'
		self.load_weights_and_biases()

		'Get lists of etf and eft that are used in forward and backward propagation'
		self.return_etf_eft()
		'Reorder so that format will be appropriate for this type of learning algorithm'
		self.reorder_neurons()

		'Now we are ready for forward/backward prop (ie. learning...)'
		print ""
		print "Welcome to the Neural Network version of the BA graph you generated\n"
		print "Usage is: '<class_instance_name>.train(num_training_images,num_epochs)'\n"
		print "Make sure you use an amount of training images <= 50000\n"
		print "Number of epochs is the amount of times you want to loop through\n"
		print "To check results against test set, call:\n"
		print "'<class_instance_name>.test_with_test_images()'\n"

	def load_weights_and_biases(self):
		self.load_weights_str = self.ask_question('Do you want to load previous weights and biases?',['Y','N'])

		if self.load_weights_str in 'N':
			#Here, we are creating new weights
			self.weights_from_to=np.zeros((self.NUM_OF_NEURONS, self.NUM_OF_NEURONS))
			np.random.seed(1) #for deterministic

			for edge in self.edges_from_to:
				edge_source,edge_target=int(edge[0]),int(edge[1])
				weight = np.random.uniform(-1,1)
				self.weights_from_to[edge_source][edge_target]=weight

			#Here, we are creating new biases
			self.biases=[np.random.uniform(-1,1) for n in self.neurons]

		if self.load_weights_str in 'Y':
			#In this instance we assume that a previously-generated NN graph has been saved with appropriate weights and biases
			#For the current graph structure
			self.load_nn()

		return self

	def save_weights_and_biases(self):
			#Save weights
			file = open('weights_from_to.txt','w')
			file.write(cPickle.dumps(self.weights_from_to))
			file.close()

			#Save biases
			file = open('biases.txt','w')
			file.write(cPickle.dumps(self.biases))
			file.close()
	
	def import_training_data(self):
		self.MNIST_TEST_IMAGES  = np.load('MNIST_TEST_IMAGES.npy')
		self.MNIST_TEST_LABELS  = np.load('MNIST_TEST_LABELS.npy')
		self.MNIST_TRAIN_IMAGES = np.load('MNIST_TRAIN_IMAGES.npy')
		self.MNIST_TRAIN_LABELS = np.load('MNIST_TRAIN_LABELS.npy')

		self.NUM_TESTING_EXAMPLES = len(self.MNIST_TEST_IMAGES)

		return self

	def save_nn(self):
		now = datetime.datetime.now()
		out_fn = 'BA_NN' + now.strftime("%Y-%m-%d %H:%M")
		file = open(out_fn+'.txt','w')
		file.write(cPickle.dumps(self.__dict__))
		file.close()

	def load_nn(self):
		files = [f for f in os.listdir('.') if os.path.isfile(f)]
		qualifying_files = [f for f in files if 'BA_NN' in f and '.txt' in f]
		qualifying_fns=[f[len('BA_NN'):(len(f)-len('.txt'))] for f in qualifying_files]
		datetime_objects = [datetime.datetime.strptime(f, "%Y-%m-%d %H:%M") for f in qualifying_fns]
		chosen_index=datetime_objects.index(max(datetime_objects))
		chosen_fn = qualifying_files[chosen_index]
		f = open(chosen_fn, 'rb')
		tmp_dict = cPickle.load(f)
		f.close()          
		self.__dict__.update(tmp_dict) 
		return self

	def return_etf_eft(self):
		self.eft=[]
		self.etf=[]

		for n in range(self.NUM_OF_NEURONS):
			self.eft.append([])
			self.etf.append([])

		for k in range(self.NUM_OF_NEURONS):
			eft_list=[]
			etf_list=[]
			for e in self.edges_from_to:
				if e[0]==k:
					eft_list.append(e[1])

			for e in self.edges_to_from:
				if e[0]==k:
					etf_list.append(e[1])


			self.eft[k]=eft_list
			self.etf[k]=etf_list

		return self

	def reorder_neurons(self):
		#we need to do this to make sure that all neurons for forward and backward propagation have
		#been correctly ordered
		for n1 in range(10,len(self.neurons)-1):
			for n2 in range(n1+1,len(self.neurons)):
				if len(self.etf[self.neurons[n1]])==0:
					if len(self.etf[self.neurons[n2]])>0:
						a, b = self.neurons.index(n1), self.neurons.index(n2)
						self.neurons[b], self.neurons[a] = self.neurons[a], self.neurons[b]
		return self

	#----------------------------------------------------------
	#UX functions (asking questions (user input) and any GUIs as appropriate)
	#----------------------------------------------------------
	
	def return_question_options(self,valid_responses):
		out_str = "("
		for response in valid_responses:
			out_str = out_str + response + '/'
		out_str = out_str[:-1]
		out_str = out_str + ")"
		return out_str

	def ask_question(self,question, valid_responses):
		output_str = ''
		valid_responses=[str(a).upper() for a in valid_responses]
		question_str = question + " " + self.return_question_options(valid_responses) + " : "
		while output_str not in valid_responses:
			output_str = str(raw_input(question_str)).upper()
			if not any([x in output_str for x in valid_responses]):
				print 'Error please select ' + self.return_question_options(valid_responses)[1:-1] + ' only!'
				print 'You selected: ' + output_str
		return output_str

	#----------------------------------------------------------
	#Functions for training, including backward and forward propagation
	#----------------------------------------------------------
	
	def set_training_values(self,num_training_images):
		'Initialise for learning process'
		self.OUTPUT_VERTICES=10
		self.LEARNING_RATE = -0.0005

		'Arrays used for activations, deltas, and z values'
		'Deepcopy since we dont want to modify originals'
		self.z = [0]*len(self.neurons)
		self.working_zee=copy.deepcopy(self.z)
		self.deltas=copy.deepcopy(self.z)
		self.activations=copy.deepcopy(self.z)

		'These are used for working alterations to biases and weights throughout the course of backpropagation'
		self.working_biases = copy.deepcopy(self.biases)
		self.working_weights_from_to = copy.deepcopy(self.weights_from_to)

		'Create a permutation for the MNIST images, to randomise training order'
		np.random.seed(1)
		self.reset_permutation(num_training_images)

		'Set number of training images to loop through'
		self.batch_size=num_training_images

		'Reinitialise stats'
		self.reinitialise_stats()

		'Basic stats array used for collating results'
		self.trainstats=[]

		return self

	def reset_permutation(self,num_training_images):
		self.perm = np.random.permutation(num_training_images)
		return self


	def reinitialise_stats(self):
		'Vars used for record-keeping'
		self.right=0
		self.wrong=0
		self.totnumtrial = 0

		return self

	def T(self,Z):
		return 1/(1+np.exp(-Z))

	def T_prime(self,Z):
		return self.T(Z)*(1-self.T(Z))

	def train(self,num_training_images, epochs):

		self.set_training_values(num_training_images)

		self.epochs = epochs #set 30 epochs but can go more or less

		for self.epoch in range(self.epochs):

			'Resetting for initial sweep through!'
			batch=0
			self.image_index=0

			for batch in range(self.batch_size):
				if self.totnumtrial%int(num_training_images/10)==0 and self.image_index>0:
					self.report_stats()
					self.reinitialise_stats()
				
				self.expected_vector=[]
				self.test_index=self.perm[self.image_index]
				self.totnumtrial+=1

				#Forward prop
				self.forward_prop()

				#Return expected vector
				self.return_expected_vector()

				#Backward prop
				self.backward_prop()

				#Check correctness for record-keeping
				self.check_correctness('TRAIN')
				
				#Increase index by 1
				self.image_index+=1

			#Reset permutation
			self.reset_permutation(num_training_images)

		return self

	def forward_prop(self):
		kk1=0
		kk2=0
		for n in reversed(self.neurons):        
			to_neuron=n           
			#Here, we are at an input neuron and we are just traversing the 28x28 grid
			if len(self.etf[n])==0: 
				if kk1==28:
					kk2+=1
					kk1=0

				self.activations[n]=self.MNIST_TRAIN_IMAGES[self.test_index][kk1][kk2]
				kk1+=1	
			#Here, we are at a non-input neuron
			else: 
				self.working_zee[to_neuron] = 0
				for from_neuron in self.etf[to_neuron]:
					self.working_zee[to_neuron]=self.working_zee[to_neuron]+self.activations[from_neuron]*self.working_weights_from_to[from_neuron][to_neuron]
				self.working_zee[to_neuron] = self.working_zee[to_neuron] + self.working_biases[to_neuron]
				self.activations[to_neuron]=self.T(self.working_zee[to_neuron])


		return self

	def return_expected_vector(self):
		self.expected_vector=[]
		for ov in range(self.OUTPUT_VERTICES):
			self.expected_vector.append(0)
		self.expected_vector[int(self.MNIST_TRAIN_LABELS[self.test_index])]=1
		return self

	def backward_prop(self):
		'1st Step: calculate deltas for each neuron'
		for from_neuron in self.neurons:
			delta=0
			if from_neuron<10:
				delta=self.expected_vector[from_neuron]-self.activations[from_neuron]
				self.deltas[from_neuron]=delta#*T_prime(zee[from_neuron]) #for cross-entropy method

			if from_neuron>=10:
				for to_neuron in self.eft[from_neuron]:
					delta = delta + self.working_weights_from_to[from_neuron][to_neuron]*self.deltas[to_neuron]
				delta=delta*self.T_prime(self.working_zee[from_neuron])
				self.deltas[from_neuron]=delta

		'2nd Step: Change biases'
		for n in self.neurons:
			self.working_biases[n]=self.working_biases[n]-self.LEARNING_RATE*self.deltas[n]

		'3rd Step: Change weights'	
		for edge in self.edges_from_to:
			from_neuron=int(edge[0])
			to_neuron=int(edge[1])
			change = self.activations[from_neuron]*self.deltas[to_neuron]
			self.working_weights_from_to[from_neuron][to_neuron]=self.working_weights_from_to[from_neuron][to_neuron]-self.LEARNING_RATE*change
		
		return self

	def check_correctness(self,TEST_OR_TRAIN):
		'5th Step: Calculate outcomes'
		self.activsub=[]
		for k in range(10):
			self.activsub.append(self.activations[k])

		maxmat=max(self.activsub)
		index_calc = self.activsub[np.argmax(self.activsub)]
		index_exp = self.expected_vector[np.argmax(self.expected_vector)]

		for mm in range(len(self.activsub)):
			if self.activsub[mm]==index_calc:
				index_mat_calc=mm

		for mm in range(len(self.expected_vector)):
			if self.expected_vector[mm]==index_exp:
				index_mat_exp=mm

		if index_mat_calc==index_mat_exp:
			self.right+=1
		else:
			self.wrong+=1

		self.correct = float(self.right)/(self.totnumtrial)

		if TEST_OR_TRAIN=='TEST':
			self.teststats.append(self.correct)
		else:
			self.trainstats.append(self.correct)

		return self


	def report_stats(self):
		'Print outcomes...'

		#Need to modify activsub to print to 2 decimal places! and put some tabs in there too!
		print self.return_printable_vector(self.activsub)
		print self.return_printable_vector(self.expected_vector)
		print "The label for current images was: " + str(self.MNIST_TRAIN_LABELS[self.test_index])
		print "epoch " + str(self.epoch+1) # +1 offset cos start at 0
		print str(self.image_index) + "'th trial"
		print "percentage right: " + str(self.correct) + "\n"

		return self

	def test_with_test_images(self):
		'Resetting for initial sweep through!'
		#initialise right and wrong
		self.teststats=[]
		batch=0
		self.totnumtrial=1
		self.right=0
		self.wrong=0

		# self.NUM_TESTING_EXAMPLES is the number of images in the test set
		for image_index in range(self.NUM_TESTING_EXAMPLES):
			self.expected_vector=[]
			self.test_index=image_index #don't need to randomise permutation
			#Forward prop
			self.forward_prop()

			#Return expected vector
			self.return_expected_vector()

			#Check correctness (no backward prop required)
			self.check_correctness('TEST')
			
			#Increase index by 1 for stats reporting
			self.totnumtrial+=1

			if image_index%100==0 and image_index>0:
				print 'We have done ' + str(image_index) + ' of ' + str(self.NUM_TESTING_EXAMPLES)
				print 'So far, there are: ' + '{0:.2f}'.format(self.correct*100) + '% of cases correct'
 		return self

	@staticmethod
	def return_printable_vector(in_vec):
		OKGREEN = '\033[92m' #This is used for highlighting max val
		ENDC = '\033[0m'
		max_index = np.argmax(in_vec)
		out_vec = '['

		in_vec = [float(iv) for iv in in_vec] #ensure cast to float before printing
		for k in range(len(in_vec)):
			if k==max_index:
				out_vec = out_vec + OKGREEN + '{0:.2f}'.format(in_vec[k]) + ENDC + '\t'
			else:
				out_vec = out_vec + '{0:.2f}'.format(in_vec[k]) + '\t'
		out_vec=out_vec[:-1] #to get rid of tab
		out_vec = out_vec + ']'
		return out_vec







