'This module produces a plot of the graph using py graph tool'

#------------------------------------------------------
#Import Libraries
#------------------------------------------------------
from BA_NN_With_Nodes_Affected import *
from graph_tool.all import *


#------------------------------------------------------
#Main class declaration
#------------------------------------------------------
class Drawable_Graph(BA_NN_With_Nodes_Affected):

	def __init__(self):
		BA_NN.__init__(self)

	def draw_graph(self):
		NN_graph = Graph(directed=True)

		#book keeping so that we can draw the graph in an ordered fashion
		self.return_distance_from_end()
		self.align_input_neurons()

		'Add vertices'
		for v in self.vertices:
			NN_graph.add_vertex()

		'Add edges'

		for v in self.vertices:
			for edge in self.etf[v]:
				NN_graph.add_edge(edge,v)

		'Find num of distances from end'

		distances_from_end = [dist for dist in set(self.steps_tuple_to_end)]

		'Find overall count of each in each group...'
		tot_num_in_each_dist=[]
		for dist in distances_from_end:
			tot_num_in_each_dist.append(sum([kk==dist for kk in self.steps_tuple_to_end]))

		#Now, gotta get the spacings right...

		VERTICAL_PIXELS=8000.

		HORIZONTAL_PIXELS=8000.

		vert_spacings = [VERTICAL_PIXELS/num for num in tot_num_in_each_dist]
		print 'seed'
		print vert_spacings
		#width by height, maybe....

		horizontal_spacings = [k*HORIZONTAL_PIXELS/max(self.steps_tuple_to_end)+1000. for k in distances_from_end]

		pos = NN_graph.new_vertex_property("vector<double>")

		orig_vert_spacings = copy.deepcopy(vert_spacings) 

		for n in self.neurons:
			#set position
			n_pos_from_end = self.steps_tuple_to_end[n]
			new_pos = (horizontal_spacings[n_pos_from_end],vert_spacings[n_pos_from_end])
			print new_pos
			pos[NN_graph.vertex(n)] = new_pos
			#then update vert_spacings
			vert_spacings[n_pos_from_end]+=orig_vert_spacings[n_pos_from_end]

		graph_draw(NN_graph, vertex_size=100, pos=pos, vertex_text=NN_graph.vertex_index, vertex_font_size=18,output_size=(10000, 10000),output="NN_graph.png")

		return self

	def return_distance_from_end(self):
		'Should return the number of neurons required to traverse until you get to an output neuron, for each neuron'
		'Will thus be a tuple'
		self.return_network_properties()
		steps_tuple=[]
		for neuron in self.neurons:
			print neuron
			steps_to_end=0
			start = neuron
			ends = [neuron]
			while not any([end in self.output_neurons for end in ends]):
				ends = [self.eft[n] for n in ends]
				#Now level the ends list...
				ends = [end[0] for end in ends]
				steps_to_end += 1

			steps_tuple.append(steps_to_end)
		self.steps_tuple_to_end=steps_tuple

		return self

	def align_input_neurons(self):
		max_to_end = max(self.steps_tuple_to_end)
		for n in self.input_neurons:
			self.steps_tuple_to_end[n] = max_to_end
		return self

	def return_distance_from_start(self):
		'Should return the number of neurons required to traverse until you get to an input neuron, for each neuron'
		'Will thus be a tuple'
		self.return_network_properties()
		steps_tuple=[]
		for neuron in self.neurons:
			print neuron
			steps_to_end=0
			start = neuron
			ends = [neuron]
			while not any([end in self.output_neurons for end in ends]):

				ends = [self.etf[n] for n in ends]

				#Now level the ends list...
				ends = [end[0] for end in ends]
				print ends
				steps_to_end += 1
				print steps_to_end
			steps_tuple.append(steps_to_end)
		self.steps_tuple_to_start=steps_tuple

		return self


