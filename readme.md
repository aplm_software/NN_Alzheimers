Hi, welcome to my project to simulate the effect of preferential and random node deletion on a FFNN.

This was completed at RMIT University in Melbourne, Australia during 2016/2017. All of the source code was written by me.

To approximate the structure of the brain, a scale-free network was generated via the Barabasi-Albert process.

This was then trained as a NN on the MNIST classification task.  You can obtain the required MNIST data here: http://yann.lecun.com/exdb/mnist/

But there is also an included script which will download and format the data automatically.

This is how I set it up on my Ubuntu machine:

Make a virtualenvironment (python2.7). You need pip also.

Type in terminal from top-level directory folder: 

pip install virtualenv
virtualenv myvenv
source myvenv/bin/activate
pip install -r requirements.txt


Please note that you have to use python2.7 (NOT python3)

And then you will create a BA graph and train it and do some other cool stuff as follows. First, start a python shell by typing the magic word into a unix terminal (making sure to remember that you're still inside the virtual environment...):

python

Then exectute the following commands from inside the shell:


''

from BA_Graph import *

BA= BA_Graph()

''

You've initialised your graph! But it's only an empty object, really. At this point, you will need to specify the number of input vertices and the m number of the model, and then populate all of its connections. A type of sorcery or a bubbling brew of the dark cauldron variety.
The number of input vertices is 784 since this is the spec of the MNIST set (28 by 28 pixels = 784 pixels). The current code will NOT work with a value different to this.
The m number is the number of edges that are added at every time step for BA process, and must be >=2.

Roughly, if you choose m>=10 you will find some interesting results. Beware though that as the number of edges
becomes higher, the NN will start to encumber more of your computer's memory...I think the complexity of the graph scales exponentially with the m number.

Once you have chosen these values, type the following in the same console window:

''

BA.generate_connections()

BA.save_graph()

from BA_NN import *

BA_NN = BA_NN()

''

Follow the prompts to either restore old values for weights and biases, or create random ones. You might want to restore old weights and biases if eg you had trained up a network previously and you wanted to return to an earlier state of learning.

The next stage in the process of creating a new graph is to train the network.

Type the following command into the terminal:

''

BA_NN.train(number_of_training_images, number_of_epochs)

''

At this point the program will cycle through the specified amount of training images for the specified number of epochs. The number of training images is is 50000. If you want to use all images then specify this as '50000'.

Then, you can test the program on the test image set by calling:

''

BA_NN.test_with_test_images()


''

This will cycle through all 10000 test images and give you a score for the total number of correct classifications.

To save current training, call:

''

BA_NN.save_nn()

''

The implementations to remove or add new neurons are in the file 'BA_NN_With_Nodes_Affected.py'

To run, you will need to call in a python shell from the working directory:

''

from BA_NN_With_Nodes_Affected() import *

BA_NN = BA_NN_With_Nodes_Affected()

''


Then you can call 'BA_NN.add_neuron()' or 'BA_NN.remove_neuron()' to affect the network, save with 'BA_NN.save_nn()', and then retrain or
test the performance.

Note that random deletion of neurons has not been implemented in the current version, only preferential deletion.

Finally, you can plot the network to get a better understanding of its structure.  

You will need to install python graph tool from here: https://graph-tool.skewed.de/

I don't think it has a pip distribution unfortunately.

To draw the graph, in a python shell, call the following, but only after you have created the network via BA_NN.py,

''

from NN_Graph_Draw import *

NN_graph = Drawable_Graph()

NN_graph.draw_graph()

''

This will draw the graph in your working directory as png image. This process can take up heaaaaaps of memory though - be careful.
