#Preliminary: Download MNIST data and reshape into numpy, save, and then delete .gz files

import wget
import gzip
import os
import numpy as np


IMAGE_SIZE = 28
NUM_TRAIN = 50000
NUM_TEST = 10000

train_img_url='http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz'
train_lbl_url='http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz'
test_img_url='http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz'
test_lbl_url='http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz'


def conv_gz_img_to_npy(fn,num_images, out_fn):
	gz_f = gzip.open(fn,'r')

	gz_f.read(16)

	buf = gz_f.read(IMAGE_SIZE * IMAGE_SIZE * num_images)
	img = np.frombuffer(buf, dtype=np.uint8).astype(np.float32)
	img= img.reshape(num_images, IMAGE_SIZE, IMAGE_SIZE, 1)

	np.save(out_fn + '.npy',img)


def conv_gz_lbl_to_npy(fn,num_labels, out_fn):

	gz_f = gzip.open(fn,'r')
	gz_f.read(8)
	buf = gz_f.read(1 * num_labels)
	labels = np.frombuffer(buf, dtype=np.uint8).astype(np.int64)

	np.save(out_fn + '.npy',labels)

print 'Downloading training/test data....'

#1. train images

print 'Downloading training images (Step 1 of 4)'
wget.download(train_img_url,'./train_img.gz')
conv_gz_img_to_npy('./train_img.gz',NUM_TRAIN, 'MNIST_TRAIN_IMAGES')

#2. test images

print 'Downloading training images (Step 2 of 4)'
wget.download(test_img_url,'./test_img.gz')
conv_gz_img_to_npy('./test_img.gz',NUM_TEST, 'MNIST_TEST_IMAGES')

#3. train labels

print 'Downloading training images (Step 3 of 4)'
wget.download(train_lbl_url,'./train_lbl.gz')
conv_gz_lbl_to_npy('./train_lbl.gz',NUM_TRAIN, 'MNIST_TRAIN_LABELS')

#4. test labels

print 'Downloading training images (Step 4 of 4)'
wget.download(test_lbl_url,'./test_lbl.gz')
conv_gz_lbl_to_npy('./test_lbl.gz',NUM_TEST, 'MNIST_TEST_LABELS')




#now delete orig .gz files :)

files = os.listdir('.')

delete_files = [f for f in files if '.gz' in f]

for delete_file in delete_files:
	os.remove(delete_file)


#now ready for training...